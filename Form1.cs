﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MovieManagementSystem
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            if (!MongoDbHelper.ConnectDatabase())
            {
                MessageBox.Show("Could not connect to mongo database!");
                return;
            }

            var movie = "{ \"Title\":\"Interstellar\", \"ProductionYear\":2014, \"Director\":\"Christopher Nolan\"}";
            MongoDbHelper.InsertMovie(movie);
        }
    }
}
