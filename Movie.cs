﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;

namespace MovieManagementSystem
{
    [BsonIgnoreExtraElements]
    public class Movie
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        private string m_title;
        public string Title
        {
            set { m_title = value; }
            get { return m_title; }
        }

        private int m_productionYear;
        public int ProductionYear
        {
            set { m_productionYear = value; }
            get { return m_productionYear; }
        }

        private string m_director;
        public string Director
        {
            set { m_director = value; }
            get { return m_director; }
        }
        /*
        private List<string> m_actors;
        [BsonIgnore]
        public List<string> Actors
        {
            get { return m_actors; }
        }

        public void AddActor(string actor)
        {
            if (!m_actors.Contains(actor))
                m_actors.Add(actor);
        }

        public void RemoveActor(string actor)
        {
            if (m_actors.Contains(actor))
                m_actors.Remove(actor);
        }
        */
    }
}
