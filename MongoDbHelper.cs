﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson.IO;
using MongoDB.Driver;
using Newtonsoft.Json;

namespace MovieManagementSystem
{
    public class MongoDbHelper
    {
        private static string m_databaseConnectString = "mongodb://127.0.0.1:27017";
        private static string m_databaseName = "mms";
        private static string m_collectionName = "movie";
        private static IMongoDatabase m_database = null;
        private static IMongoClient m_mongoClient = null;
        private static IMongoCollection<Movie> m_movieCollection = null;

        public static bool ConnectDatabase()
        {
            m_mongoClient = new MongoClient(m_databaseConnectString);
            m_database = m_mongoClient.GetDatabase(m_databaseName);

            return true;
        }

        private static void GetMovieCollection()
        {
            m_movieCollection = m_database.GetCollection<Movie>(m_collectionName);
        }

        public static void InsertMovie(string movieJsonString)
        {
            if (m_movieCollection == null)
                GetMovieCollection();

            var jSetting = new JsonSerializerSettings();
            jSetting.NullValueHandling = NullValueHandling.Ignore;

            // Convert json string into json object
            var movieJsonObj = Newtonsoft.Json.JsonConvert.DeserializeObject<Movie>(movieJsonString, jSetting);

            using (var session = m_mongoClient.StartSession())
            {
                try
                {
                    session.StartTransaction();

                    // Insert into collection
                    m_movieCollection.InsertOneAsync(movieJsonObj).GetAwaiter().GetResult();
                }
                catch (Exception exception)
                {
                    Console.WriteLine($"Caught exception during transaction, aborting: {exception.Message}.");
                    session.AbortTransaction();
                }
            }
        }
    }
}
